/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   puiss4.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ibuchwal <ibuchwal@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/08 17:03:11 by ibuchwal          #+#    #+#             */
/*   Updated: 2014/03/09 07:00:08 by ibuchwal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUISS4_H
# define PUISS4_H

# include <libft.h>
# include <time.h>

# define STDIN	0
# define STDOUT	1
# define STDERR	2
# define PUISS4					"puissance4"
# define ERR_SEPARATOR			": "
# define ERR_TOO_MANY_ARGS		"Too many arguments"
# define ERR_TOO_FEW_ARGS		"Too few arguments"
# define ERR_GRID_SIZE			"Wrong grid size"
# define PLAYER_FREE 0
# define PLAYER_1 1
# define PLAYER_2 2
# define AI_PRECISION 2
# define FULL_GAME -10
# define WRITE(fd, what) (write(fd, what, sizeof(what) - 1))

typedef struct s_grid	t_grid;

struct 		s_grid
{
	int		columns;
	int		lines;
	int		**tab;
};

int		ia_check_line_right(t_grid grid);
int		ia_check_line_left(t_grid grid);
int		ia_check_column(t_grid grid);
int		ia_check_diag_left(t_grid grid);
int		ia_check_diag_right(t_grid grid);
void	ia_play(t_grid grid);


int		check_line(t_grid grid);
int		check_column(t_grid grid);
int		check_diag_right(t_grid grid);
int		check_diag_left(t_grid grid);
int		check_full(t_grid grid);
void	init_grid(t_grid *grid, char **argv);
void	display_grid(t_grid grid);
int		add_chip(t_grid *grid, int where, int player);
void	error(const char *name, const char *error, const char *info);

#endif /* PUISS4_H */
