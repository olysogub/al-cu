/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ibuchwal <ibuchwal@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/30 14:15:15 by ibuchwal          #+#    #+#             */
/*   Updated: 2014/03/08 17:01:51 by ibuchwal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *s1, const void *s2, size_t n)
{
	void	*temp;

	temp = malloc(n);
	if (temp)
	{
		ft_memcpy(temp, s2, n);
		ft_memcpy(s1, temp, n);
		free(temp);
	}
	return (s1);
}
