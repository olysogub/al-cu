/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstappend.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ibuchwal <ibuchwal@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/30 14:15:15 by ibuchwal          #+#    #+#             */
/*   Updated: 2014/03/08 17:02:00 by ibuchwal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstappend(t_list **alst, t_list *new_element)
{
	t_list	*it;

	if (!*alst)
		*alst = new_element;
	else
	{
		it = *alst;
		while (it->next)
			it = it->next;
		it->next = new_element;
	}
}
