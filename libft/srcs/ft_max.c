/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_max.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ibuchwal <ibuchwal@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/16 05:10:41 by ibuchwal          #+#    #+#             */
/*   Updated: 2014/03/08 17:01:55 by ibuchwal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_max(int a, int b)
{
	if (a > b)
		return (a);
	return (b);
}

size_t	ft_umax(size_t a, size_t b)
{
	if (a > b)
		return (a);
	return (b);
}
