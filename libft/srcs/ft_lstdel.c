/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ibuchwal <ibuchwal@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/30 14:15:15 by ibuchwal          #+#    #+#             */
/*   Updated: 2014/03/08 17:02:00 by ibuchwal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstdel(t_list **alst, t_deleter del)
{
	t_list	*it;
	t_list	*temp;

	if (alst)
	{
		it = *alst;
		while (it)
		{
			del(it->content, it->content_size);
			temp = it;
			it = it->next;
			free(temp);
		}
		*alst = 0;
	}
}
