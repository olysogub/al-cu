/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ibuchwal <ibuchwal@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/26 15:03:10 by ibuchwal          #+#    #+#             */
/*   Updated: 2014/03/08 17:01:37 by ibuchwal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strncpy(char *dst, const char *src, size_t n)
{
	char	*idst;

	idst = dst;
	while (*src && n)
	{
		*idst++ = *src++;
		--n;
	}
	while (n--)
		*idst++ = '\0';
	return (dst);
}
