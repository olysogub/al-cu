/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dlist_delete.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ibuchwal <ibuchwal@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/07 12:18:50 by ibuchwal          #+#    #+#             */
/*   Updated: 2014/03/08 17:02:06 by ibuchwal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_dlist_delete(t_dlist **head, t_deleter del)
{
	t_dlist	*it;
	t_dlist	*tmp;

	if (*head)
	{
		it = (*head)->previous;
		if (it)
		{
			while (it != *head)
			{
				tmp = it->previous;
				del(it->content, it->content_size);
				free(it);
				it = tmp;
			}
			del(it->content, it->content_size);
			free(it);
		}
		*head = 0;
	}
}
