/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ibuchwal <ibuchwal@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/21 12:09:16 by ibuchwal          #+#    #+#             */
/*   Updated: 2014/03/08 17:01:52 by ibuchwal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *s1, const void *s2, size_t n)
{
	char		*str1;
	const char	*str2;

	str1 = (char *)s1;
	str2 = (const char *)s2;
	while (n--)
		str1[n] = str2[n];
	return (s1);
}
