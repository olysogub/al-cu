/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ibuchwal <ibuchwal@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/30 14:15:15 by ibuchwal          #+#    #+#             */
/*   Updated: 2014/03/08 17:01:40 by ibuchwal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmapi(const char *s, char (*f)(size_t, char))
{
	char	*mapped;
	size_t	i;

	if (!s)
		return (0);
	mapped = ft_strnew(ft_strlen(s));
	if (mapped)
	{
		i = 0;
		while (*s)
		{
			mapped[i] = f(i, *s++);
			++i;
		}
	}
	return (mapped);
}
