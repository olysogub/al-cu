/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ibuchwal <ibuchwal@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/21 13:34:28 by ibuchwal          #+#    #+#             */
/*   Updated: 2014/03/08 17:01:35 by ibuchwal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strrchr(const char *s, int c)
{
	char		cc;
	const char	*last_occurence;

	cc = (char)c;
	last_occurence = 0;
	while (*s)
	{
		if (*s == cc)
			last_occurence = s;
		++s;
	}
	if (*s == cc)
		last_occurence = s;
	return ((char*)last_occurence);
}
