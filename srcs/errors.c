/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   errors.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ibuchwal <ibuchwal@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/08 17:29:31 by ibuchwal          #+#    #+#             */
/*   Updated: 2014/03/09 04:18:28 by ibuchwal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <puiss4.h>

void	error(const char *name, const char *error, const char *info)
{
	ft_putstr_fd(name, STDERR);
	write(STDERR, ERR_SEPARATOR, ft_strlen(ERR_SEPARATOR));
	ft_putstr_fd(error, STDERR);
	if (info)
	{
		write(STDERR, ERR_SEPARATOR, ft_strlen(ERR_SEPARATOR));
		ft_putstr_fd(info, STDERR);
	}
	ft_putchar_fd('\n', STDERR);
	exit(0);
}
