/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ibuchwal <ibuchwal@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/08 22:45:21 by ibuchwal          #+#    #+#             */
/*   Updated: 2014/03/09 06:49:12 by ibuchwal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <puiss4.h>

void	ia_play(t_grid grid)
{
	int	j;

	if ((j = ia_check_column(grid)) >= 0)
		add_chip(&grid, j, 2);
	else if ((j = ia_check_diag_left(grid)) >= 0)
		add_chip(&grid, j, 2);
	else if ((j = ia_check_diag_right(grid)) >= 0)
		add_chip(&grid, j, 2);
	else if ((j = ia_check_line_right(grid)) >= 0)
		add_chip(&grid, j, 2);
	else if ((j = ia_check_line_left(grid)) >= 0)
		add_chip(&grid, j, 2);
	else
		add_chip(&grid, rand() % grid.columns, 2);
}

int		plays(int i, t_grid grid)
{
	char	*line;

	if (i % 2 + 1 == 2)
	{
		ft_putstr("IA plays : \n");
		ia_play(grid);
		i++;
	}
	else
	{
		ft_putstr("Player plays : ");
		get_next_line(0, &line);
		if (add_chip(&grid, ft_atoi(line) - 1, 1))
			i++;
		free(line);
	}
	return (i);
}

int		checks(t_grid grid, int i)
{
	if (check_line(grid) || check_column(grid) || check_diag_left(grid)
		|| check_diag_right(grid))
	{
		if ((i - 1) % 2 + 1 == 2)
			ft_putendl("Computer wins, lol noob. Such IA.");
		else
			ft_putendl("Player wins ! FINISH HIM !");
		return (1);
	}
	if (check_full(grid))
	{
		ft_putendl("Draw, everybody lose");
		return (1);
	}
	return (0);
}

int		main(int argc, char **argv)
{
	int		i;
	t_grid	grid;

	srand(time(0));
	if (argc == 3)
	{
		i = rand() % 42;
		init_grid(&grid, argv);
		display_grid(grid);
		while (1)
		{
			i = plays(i, grid);
			display_grid(grid);
			if (checks(grid, i))
				return (0);
		}
	}
	else if (argc < 3)
		error(PUISS4, ERR_TOO_FEW_ARGS, 0);
	else
		error(PUISS4, ERR_TOO_MANY_ARGS, 0);
	return (0);
}
