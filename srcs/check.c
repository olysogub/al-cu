/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ibuchwal <ibuchwal@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/08 23:31:07 by ibuchwal          #+#    #+#             */
/*   Updated: 2014/03/09 06:38:22 by ibuchwal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <puiss4.h>

int		check_line(t_grid grid)
{
	int	i;
	int	j;

	i = 0;
	while (i < grid.lines)
	{
		j = 0;
		while (j < grid.columns - 3)
		{
			if (grid.tab[i][j]
				&& grid.tab[i][j] == grid.tab[i][j + 1]
				&& grid.tab[i][j] == grid.tab[i][j + 2]
				&& grid.tab[i][j] == grid.tab[i][j + 3])
				return (1);
			j++;
		}
		i++;
	}
	return (0);
}

int		check_column(t_grid grid)
{
	int	i;
	int	j;

	i = 0;
	while (i < grid.lines - 3)
	{
		j = 0;
		while (j < grid.columns)
		{
			if (grid.tab[i][j]
				&& grid.tab[i + 1][j] == grid.tab[i][j]
				&& grid.tab[i + 2][j] == grid.tab[i][j]
				&& grid.tab[i + 3][j] == grid.tab[i][j])
				return (1);
			j++;
		}
		i++;
	}
	return (0);
}

int		check_diag_left(t_grid grid)
{
	int	i;
	int	j;

	i = 0;
	while (i < grid.lines - 3)
	{
		j = 0;
		while (j < grid.columns - 3)
		{
			if (grid.tab[i][j]
				&& grid.tab[i + 1][j + 1] == grid.tab[i][j]
				&& grid.tab[i + 2][j + 2] == grid.tab[i][j]
				&& grid.tab[i + 3][j + 3] == grid.tab[i][j])
				return (1);
			j++;
		}
		i++;
	}
	return (0);
}

int		check_diag_right(t_grid grid)
{
	int	i;
	int	j;

	i = 0;
	while (i < grid.lines - 3)
	{
		j = 0;
		while (j < grid.columns)
		{
			if (grid.tab[i][j]
				&& grid.tab[i + 1][j - 1] == grid.tab[i][j]
				&& grid.tab[i + 2][j - 2] == grid.tab[i][j]
				&& grid.tab[i + 3][j - 3] == grid.tab[i][j])
				return (1);
			j++;
		}
		i++;
	}
	return (0);
}

int		check_full(t_grid grid)
{
	int	i;

	i = 0;
	while (i < grid.columns)
	{
		if (!grid.tab[0][i])
			return (0);
		i++;
	}
	return (1);
}
