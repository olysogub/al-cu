/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   puiss4.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ibuchwal <ibuchwal@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/09 06:39:10 by ibuchwal          #+#    #+#             */
/*   Updated: 2014/03/09 06:59:34 by ibuchwal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <puiss4.h>

void	init_grid(t_grid *grid, char **argv)
{
	int	i;
	int	j;

	grid->lines = ft_atoi(argv[1]);
	grid->columns = ft_atoi(argv[2]);
	if (grid->lines < 6)
		error(PUISS4, ERR_GRID_SIZE, 0);
	if (grid->columns < 7)
		error(PUISS4, ERR_GRID_SIZE, 0);
	grid->tab = (int**)malloc(sizeof(int) * grid->columns * grid->lines);
	i = 0;
	while (i < grid->lines)
	{
		j = 0;
		grid->tab[i] = (int*)ft_memalloc(sizeof(int) * grid->columns);
		while (j < grid->columns)
		{
			grid->tab[i][j] = 0;
			j++;
		}
		i++;
	}
}

void	display_border(t_grid grid)
{
	int	i;
	int	j;

	i = 0;
	j = 0;
	while (i <= grid.columns * 2)
	{
		if (i % 2 == 1)
		{
			ft_putnbr((j + 1) % 10);
			j++;
		}
		else
			write(1, "#", 1);
		i++;
	}
	write(1, "\n", 1);
}

void	display_grid(t_grid grid)
{
	int		i;
	int		j;
	char	c;

	i = 0;
	display_border(grid);
	while (i < grid.lines)
	{
		write(1, "|", 1);
		j = 0;
		while (j < grid.columns)
		{
			c = grid.tab[i][j++] + ' ';
			write(1, &c, 1);
			write (1, "|", 1);
		}
		write (1, "\n", 1);
		i++;
	}
	display_border(grid);
}

int		add_chip(t_grid *grid, int where, int player)
{
	int	i;

	if (where < 0 || where >= grid->columns)
		return (0);
	if (grid->tab[0][where])
		return (0);
	i = grid->lines - 1;
	while (grid->tab[i][where] != 0)
		i--;
	grid->tab[i][where] = (player == 1 ? 47 : 56);
	return (1);
}
