/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ia.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ibuchwal <ibuchwal@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/09 05:26:13 by ibuchwal          #+#    #+#             */
/*   Updated: 2014/03/09 07:01:59 by ibuchwal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <puiss4.h>

int		ia_check_line_right(t_grid grid)
{
	int	i;
	int	j;

	i = 0;
	while (i < grid.lines)
	{
		j = 0;
		while (j < grid.columns - 3)
		{
			if (grid.tab[i][j]
				&& grid.tab[i][j] == grid.tab[i][j + 1]
				&& grid.tab[i][j] == grid.tab[i][j + 2]
				&& grid.tab[i][j + 3] == 0)
				return (j + 3);
			j++;
		}
		i++;
	}
	return (-1);
}

int		ia_check_line_left(t_grid grid)
{
	int	i;
	int	j;

	i = 0;
	while (i < grid.lines)
	{
		j = grid.columns - 1;
		while (j >= 3)
		{
			if (grid.tab[i][j]
				&& grid.tab[i][j] == grid.tab[i][j - 1]
				&& grid.tab[i][j] == grid.tab[i][j - 2]
				&& grid.tab[i][j - 3] == 0)
				return (j - 3);
			j--;
		}
		i++;
	}
	return (-1);
}

int		ia_check_column(t_grid grid)
{
	int	i;
	int	j;

	i = grid.lines - 1;
	while (i >= 3)
	{
		j = 0;
		while (j < grid.columns)
		{
			if (grid.tab[i][j]
				&& grid.tab[i - 1][j] == grid.tab[i][j]
				&& grid.tab[i - 2][j] == grid.tab[i][j]
				&& grid.tab[i - 3][j] == 0)
				return (j);
			j++;
		}
		i--;
	}
	return (-1);
}

int		ia_check_diag_left(t_grid grid)
{
	int	i;
	int	j;

	i = grid.lines - 1;
	while (i >= 3)
	{
		j = grid.columns;
		while (j > 3)
		{
			if (grid.tab[i][j]
				&& grid.tab[i - 1][j - 1] == grid.tab[i][j]
				&& grid.tab[i - 2][j - 2] == grid.tab[i][j]
				&& grid.tab[i - 3][j - 3] == 0)
				return (j - 3);
			j--;
		}
		i--;
	}
	return (-1);
}

int		ia_check_diag_right(t_grid grid)
{
	int	i;
	int	j;

	i = grid.lines - 1;
	while (i >= 3)
	{
		j = 0;
		while (j < grid.columns - 3)
		{
			if (grid.tab[i][j]
				&& grid.tab[i - 1][j + 1] == grid.tab[i][j]
				&& grid.tab[i - 2][j + 2] == grid.tab[i][j]
				&& grid.tab[i - 3][j + 3] == 0)
				return (j + 3);
			j++;
		}
		i--;
	}
	return (-1);
}
